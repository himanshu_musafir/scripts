﻿
$path = "D:\Scripts\BuildScripts-WebApi\Musafir.Development.Git.xml"
[xml]$init=get-content -path $path

$config = "D:\Scripts\BuildScripts-WebApi\env1.config"
[xml] $cfg = Get-Content -Path $config
$DeployPath = $cfg.configuration.appSettings.DeploymentRoot.path
$PhysicalPath = $cfg.configuration.appSettings.DeploymentPath.path
$publishroot = $cfg.configuration.appSettings.PublishRoot.path
$ipaddress = $cfg.configuration.appSettings.DefaultWebsiteIP.ip
$domainusername = 'musafiralpha\himanshu'
$domainpassword = '!2o07XNg6W60Sr2'

Set-Alias CreateIISSite D:\Scripts\BuildScripts-WebApi\CreateSite.ps1

forEach ($site in $init.Repository.CodeFolder)
{
    foreach($market in $site.MarketProfileConfiguration)
    {
    $siteName = $init.Repository.Branch.Split('\')
    $websiteName = $site.Name + '.musafir-' + $siteName[1] + '-' + $market.MarketProfile.Code + '.' +  $cfg.configuration.appSettings.DnsZone.value
    $httpport = $market.HttpPort
    $httpsport = $market.HttpsPort
    $marketid = $market.MarketProfile.id
    $marketCode = $market.MarketProfile.Code
    $payment = $market.AssociatedServer
    $publishpath = $publishroot +  "\Full\" + $site.Name
    $sitepath = $PhysicalPath + '\' + $websiteName
     
    if(Test-Path -path $DeployPath\$websiteName)
    {
    #Remove-Item -Path $DeployPath\$websiteName\* -Recurse -Force
    Copy-Item -path $publishpath\* -Destination $DeployPath\$websiteName -Recurse -Force
   
    }
    else
   {
     New-Item -ItemType Directory $DeployPath\$websiteName
     Copy-Item -path $publishpath\* -Destination $DeployPath\$websiteName -Recurse -Force
    }
  
    #check if virtual application exists 
    $s= $site.Name
        if($init.Repository.Website.$s.vApp.name)
        {
        
        $vApp = $init.Repository.Website.$s.vApp.name
        $vpath = $sitepath + '\' + $vapp

        #CreateIISSite -siteName $websiteName -appPoolName $websiteName -port $httpport -httpsport $httpsport -ipaddress $ipaddress -path $sitepath -vApp $vApp -vpath $vPath -domainusername $domainusername -domainpassword $domainpassword

        }
       #CreateIISSite -siteName $websiteName -appPoolName $websiteName -port $httpport -httpsport $httpsport -ipaddress $ipaddress -path $sitepath -domainusername $domainusername -domainpassword $domainpassword
     
     #update web config
   
  $codePath = $init.Repository.Path + '\' + '\' + $init.Repository.Website.$s.folder
    if($s -eq 'web')
    {
     if(!(Test-path $DeployPath\$websiteName\web.config))
     {
     $webConfigRoot = $codePath + '\' + 'web.config.alpha.' + $marketCode.ToLower()
     Copy-Item -Path $webConfigRoot -Destination $DeployPath\$websiteName -Force -PassThru | Rename-Item -NewName 'web.config' -Force -Verbose 
     }
    }
    elseif($s -eq 'api')
    {
        if(!(Test-path $DeployPath\$websiteName\web.config))
        {
        $webConfigRoot = $codePath + '\' + 'web.config.full.development.' + $marketCode.ToLower()
        Copy-Item -Path $webConfigRoot -Destination $DeployPath\$websiteName -Force -PassThru | Rename-Item -NewName 'web.config' -Force -Verbose 
        }
    }

     [xml] $webconfig = Get-Content -Path $DeployPath\$websiteName\web.config
     $MarketProfileHttpPortOverride = $webConfig.configuration.appSettings.add | where { $_.key -eq 'MarketProfileHttpPortOverride'}
     $MarketProfileHttpPortOverride.value = $httpPort
     $MarketProfileHttpsPortOverride = $webConfig.configuration.appSettings.add | where { $_.key -eq 'MarketProfileHttpsPortOverride'}
     $MarketProfileHttpsPortOverride.value = $httpsPort
     $MarketProfileDomainNameOverride = $webConfig.configuration.appSettings.add | where { $_.key -eq 'MarketProfileDomainNameOverride'}
     $MarketProfileDomainNameOverride.value = $cfg.configuration.appSettings.DnsZone.value
     $DefaultMarketProfileID = $webConfig.configuration.appSettings.add | where { $_.key -eq 'DefaultMarketProfileID'}
     $DefaultMarketProfileID.value = $marketid
     $PaymentSecureServerDomainName = $webConfig.configuration.appSettings.add | where { $_.key -eq 'PaymentSecureServerDomainName'}
     $PaymentSecureServerDomainName.value = $payment
     $webconfig.Save("$DeployPath\$websiteName" + '\' + 'web.config')
    }
    
}


