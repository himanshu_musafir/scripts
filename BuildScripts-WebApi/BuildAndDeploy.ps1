﻿

function aspnetCompile ( $virtualPath, $codePath, $publishPath)
{
#Exclude Global.asax
Write-Host " Excluding Global.asax"

   if(Test-Path $codePath\Global.asax)
   {
    try
    {
        Rename-Item $codePath\Global.asax Global.asax.exclude
    }
    catch
    {
        write-host "Global.asax already excluded"
    }
   }
   if(!(Test-Path $codePath\web.config))
   {
   Rename-Item $codePath\web.config.alpha.in web.config
   }
Write-Host " Compiling $codePath"

$aspnetCompiler = (Join-Path $env:windir 'Microsoft.NET\Framework\v4.0.30319\aspnet_compiler')
#& $aspnetCompiler -p "F:\Code\web\Musafir.Presentation.Website" -v /-u "F:\Code\web\Musafir.Presentation.Website.PublishSite"
 & $aspnetCompiler -f -v /$virtualPath -p $codePath -u -c -d $publishPath\$virtualPath

}

function aspnetMerge ($publishPath, $websitename, $virtualPath)
{
$aspMerge = "C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\aspnet_merge.exe"
& $aspMerge $publishPath -o $websitename -a -debug

Write-Host "Copy Global.asax to publish path"
    if(!(Test-Path $codePath\Global.asax))
    {
        Copy-Item $codePath\Global.asax.exclude -Destination $publishPath\$virtualPath 
        Rename-Item $publishPath\$virtualPath\Global.asax.exclude Global.asax
        
    }
}


Clear-Host

$path = "D:\Scripts\BuildScripts-WebApi\Musafir.Development.Git.xml"
[xml]$init=get-content -path $path

$config = "D:\Scripts\BuildScripts-WebApi\env1.config"
[xml]$cfg = get-content -path $config


$DeployPath = $cfg.configuration.appSettings.DeploymentRoot.path
$PhysicalPath = $cfg.configuration.appSettings.DeploymentPath.path
$publishFolder = $cfg.configuration.appSettings.PublishRoot.path
$ipaddress = $cfg.configuration.appSettings.DefaultWebsiteIP.ip
$domainusername = 'musafiralpha\himanshu'
$domainpassword = 'Musafir@1@3$5^7'
$baseDirectory = $init.Repository.Path

forEach($n in $init.Repository.Website.ChildNodes)
    {
    $codePath = $baseDirectory + '\' + $n.folder
    $virtualPath = $n.vpath
    $publishPath = $codePath + '.Publish'
    $websitename = $n.Name + '-' + $codeFolder.Name + '.musafiralpha.local'
    aspnetCompile $virtualPath $codePath $publishPath
    if($n.vApp.folder)
        {
        Write-Host " Compiling virtual App"
        $codePath = $baseDirectory + '\' + $n.vApp.folder
        aspnetCompile $n.vApp.vpath $codePath $publishPath + '\' + $n.vApp.vpath
        aspnetMerge $publishPath + '\' + $n.vApp.vpath $websitename $virtualPath
        }

    aspnetMerge $publishPath $websitename


    $publishroot = $cfg.configuration.appSettings.PublishRoot.path + '\Full\' + $n.Name

    if(Test-path -Path $publishroot)
        {
        Remove-Item -Path $publishroot\* -Recurse -Force
        Copy-Item -path $publishPath\* -Recurse -Destination $publishroot -Force
        }
    else
        {
        New-Item -ItemType Directory $publishroot
        Copy-Item -path $publishPath\* -Recurse -Destination $publishroot -Force
        }

    # Cleanup Publish Folder
    Remove-Item $publishroot\* -Recurse -Include ('web.config*', 'PublishProfiles', 'Gruntfile.js', 'package.json', 'PrecompiledApp.config', 'bin\*.xml') -Verbose
    Remove-Item $publishroot\bin\* -Recurse -Include ('*.xml','*.config') -Verbose

    }

#Publish Package to Sites
forEach ($site in $init.Repository.CodeFolder)
{
    foreach($market in $site.MarketProfileConfiguration)
    {
    $siteName = $init.Repository.Branch.Split('\')
    $websiteName = $site.Name + '.musafir-' + $siteName[1] + '-' + $market.MarketProfile.Code + '.' +  $cfg.configuration.appSettings.DnsZone.value
    $httpport = $market.HttpPort
    $httpsport = $market.HttpsPort
    $marketid = $market.MarketProfile.id
    $marketCode = $market.MarketProfile.Code
    $payment = $market.AssociatedServer
    $publishpath = $publishFolder +  "\Full\" + $site.Name
    $sitepath = $PhysicalPath + '\' + $websiteName
     
    if(Test-Path -path $DeployPath\$websiteName)
    {
    #Remove-Item -Path $DeployPath\$websiteName\* -Recurse -Force
    #Copy-Item -path $publishpath\* -Destination $DeployPath\$websiteName -Recurse -Force
    ROBOCOPY $publishpath $DeployPath\$websiteName /MIR /XD "App_Data" "Is" "Images" "Sitefinity" /XF "Global.asax\" /XF "Robots.txt\" /XF "web.config" /njh /njs /ndl /nc /ns
   
    }
    else
   {
     New-Item -ItemType Directory $DeployPath\$websiteName
     Copy-Item -path $publishpath\* -Destination $DeployPath\$websiteName -Recurse -Force
    }
  
    #check if virtual application exists 
    $s= $site.Name
        if($init.Repository.Website.$s.vApp.name)
        {
        
        $vApp = $init.Repository.Website.$s.vApp.name
        $vpath = $sitepath + '\' + $vapp

        #CreateIISSite -siteName $websiteName -appPoolName $websiteName -port $httpport -httpsport $httpsport -ipaddress $ipaddress -path $sitepath -vApp $vApp -vpath $vPath -domainusername $domainusername -domainpassword $domainpassword

        }
       #CreateIISSite -siteName $websiteName -appPoolName $websiteName -port $httpport -httpsport $httpsport -ipaddress $ipaddress -path $sitepath -domainusername $domainusername -domainpassword $domainpassword
     
     #update web config
if(!(Test-path $DeployPath\$websiteName\web.config))
     {
  $codePath = $init.Repository.Path + '\' + '\' + $init.Repository.Website.$s.folder
    if($s -eq 'web')
    {
    
     $webConfigRoot = $codePath + '\' + 'web.config.alpha.' + $marketCode.ToLower()
     Copy-Item -Path $webConfigRoot -Destination $DeployPath\$websiteName -Force -PassThru | Rename-Item -NewName 'web.config' -Force -Verbose 
     
    }
    elseif($s -eq 'api')
    {              
        $webConfigRoot = $codePath + '\' + 'web.config.full.development.' + $marketCode.ToLower()
        Copy-Item -Path $webConfigRoot -Destination $DeployPath\$websiteName -Force -PassThru | Rename-Item -NewName 'web.config' -Force -Verbose 
        
       # Copy-Item -Path $webConfigRoot -Destination $DeployPath\$websiteName\webApi -Force -PassThru | Rename-Item -NewName 'web.config' -Force -Verbose
    }
  
     [xml] $webconfig = Get-Content -Path $DeployPath\$websiteName\web.config
     $MarketProfileHttpPortOverride = $webConfig.configuration.appSettings.add | where { $_.key -eq 'MarketProfileHttpPortOverride'}
     $MarketProfileHttpPortOverride.value = $httpPort
     $MarketProfileHttpsPortOverride = $webConfig.configuration.appSettings.add | where { $_.key -eq 'MarketProfileHttpsPortOverride'}
     $MarketProfileHttpsPortOverride.value = $httpsPort
     $MarketProfileDomainNameOverride = $webConfig.configuration.appSettings.add | where { $_.key -eq 'MarketProfileDomainNameOverride'}
     $MarketProfileDomainNameOverride.value = $cfg.configuration.appSettings.DnsZone.value
     $DefaultMarketProfileID = $webConfig.configuration.appSettings.add | where { $_.key -eq 'DefaultMarketProfileID'}
     $DefaultMarketProfileID.value = $marketid
     $PaymentSecureServerDomainName = $webConfig.configuration.appSettings.add | where { $_.key -eq 'PaymentSecureServerDomainName'}
     $PaymentSecureServerDomainName.value = $payment
     $webconfig.Save("$DeployPath\$websiteName" + '\' + 'web.config')
    }
    }
    
}


