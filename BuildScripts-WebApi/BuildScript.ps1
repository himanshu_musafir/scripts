﻿

function aspnetCompile ( $virtualPath, $codePath, $publishPath)
{
#Exclude Global.asax
Write-Host " Excluding Global.asax"

   if(Test-Path $codePath\Global.asax)
   {
    try
    {
        Rename-Item $codePath\Global.asax Global.asax.exclude
    }
    catch
    {
        write-host "Global.asax already excluded"
    }
   }
   if(!(Test-Path $codePath\web.config))
   {
   Rename-Item $codePath\web.config.alpha.in web.config
   }
Write-Host " Compiling $codePath"

$aspnetCompiler = (Join-Path $env:windir 'Microsoft.NET\Framework\v4.0.30319\aspnet_compiler')
#& $aspnetCompiler -p "F:\Code\web\Musafir.Presentation.Website" -v /-u "F:\Code\web\Musafir.Presentation.Website.PublishSite"
 & $aspnetCompiler -f -v /$virtualPath -p $codePath -u -c -d $publishPath\$virtualPath

}

function aspnetMerge ($publishPath, $websitename, $virtualPath)
{
$aspMerge = "C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\aspnet_merge.exe"
& $aspMerge $publishPath -o $websitename -a -debug

Write-Host "Copy Global.asax to publish path"
    if(!(Test-Path $codePath\Global.asax))
    {
        Copy-Item $codePath\Global.asax.exclude -Destination $publishPath\$virtualPath 
        Rename-Item $publishPath\$virtualPath\Global.asax.exclude Global.asax
        
    }
}


Clear-Host

#Write-Host "Please make sure you do not have any instances of Visual studio running before running the script..." -ForegroundColor Red -BackgroundColor White
#Write-Host "You have 10 seconds" -ForegroundColor Red -BackgroundColor White

#Start-Sleep -s 10

$path = "D:\Scripts\BuildScripts-WebApi\Musafir.Development.Git.xml"
[xml]$init=get-content -path $path

$config = "D:\Scripts\BuildScripts-WebApi\env1.config"
[xml]$cfg = get-content -path $config

$baseDirectory = $init.Repository.Path
#$solutionFilesPath = "$baseDirectory\SolutionConfig.txt"
#$projectFiles = Get-Content $solutionFilesPath 

#$msbuild = "C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
$msbuild = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe"
$MSBuildLogger="/flp1:Append;LogFile=Build.log;Verbosity=Normal; /flp2:LogFile=BuildErrors.log;Verbosity=Normal;errorsonly"

#foreach($codeFolder in $init.Repository.CodeFolder)
#{
    forEach($n in $init.Repository.Website.ChildNodes)
    {
    $codePath = $baseDirectory + '\' + $n.folder
    $virtualPath = $n.vpath
    $publishPath = $codePath + '.Publish'
    $websitename = $n.Name + '-' + $codeFolder.Name + '.musafiralpha.local'
    aspnetCompile $virtualPath $codePath $publishPath
        if($n.vApp.folder)
        {
        Write-Host " Compiling virtual App"
        $codePath = $baseDirectory + '\' + $n.vApp.folder
        aspnetCompile $n.vApp.vpath $codePath $publishPath + '\' + $n.vApp.vpath
        aspnetMerge $publishPath + '\' + $n.vApp.vpath $websitename $virtualPath
        }

    aspnetMerge $publishPath $websitename


    $publishroot = $cfg.configuration.appSettings.PublishRoot.path + '\Full\' + $n.Name

        if(Test-path -Path $publishroot)
        {
        Remove-Item -Path $publishroot\* -Recurse -Force
        Copy-Item -path $publishPath\* -Recurse -Destination $publishroot -Force
        }
        else
        {
        New-Item -ItemType Directory $publishroot
        Copy-Item -path $publishPath\* -Recurse -Destination $publishroot -Force
        }

    # Cleanup Publish Folder
    Remove-Item $publishroot\* -Recurse -Include ('web.config*', 'PublishProfiles', 'Gruntfile.js', 'package.json', 'PrecompiledApp.config', 'bin\*.xml') -Verbose
    Remove-Item $publishroot\bin\* -Recurse -Include ('*.xml','*.config') -Verbose

    }
#}