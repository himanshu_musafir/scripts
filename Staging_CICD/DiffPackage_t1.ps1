﻿param 
(
    [string]$source = $(throw '- Need path to source package'),
    [string]$destinationSite = $(throw '- Nee destinationSite path'),
    [string]$deployFolder = $(throw '- Need deployFolder path'),
    $serverIp = $(throw '- Need serverIp'),
    [string]$username = $(throw '- Need path to username'),
	[string]$password = $(throw '- Need path to password')
	
)

$securePassword = ConvertTo-SecureString $password -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential($username,$securePassword)  
$Session = new-PSSession -ComputerName $serverIp -Credential $credential  #this will store session in a variable

#Compress Full Package
Add-Type -assembly "system.io.compression.filesystem"
$SourceZip = "D:\CI\TempFolder\Full.zip"
If(Test-path $SourceZip) {Remove-item $SourceZip}
[io.compression.zipfile]::CreateFromDirectory($Source, $SourceZip)

Remove-Item $deployFolder\* -Recurse -Force -Exclude folderList.txt
Copy-Item -Recurse -Path $sourceZip -Destination $deployFolder -ToSession $session #this will copy folder to the path you will mention

Invoke-Command -Session $Session  -ScriptBlock {
param
( $deployFolder, $destinationSite, $SourceZip)

$fullPublishFolder = $deployFolder + "Full"
Write-Host $fullPublishFolder and  $deployFolder
$currentSite = $destinationSite
$diffFolder = $deployFolder + "Diff\"
$zip = $deployFolder +'Full.zip'
Write-Host $diffFolder
[string[]]$Excludes = @('App_Data', '\API\APP_Data', 'Is', 'Images','Default.aspx', 'Sitefinity','\API\Is','\API\Global.asax', '\API\web.config','Global.asax','Robots.txt', 'web.config', 'apple-app-site-association' , 'MD1133.aspx', 'Recycle.aspx' )

#Cleanup diffFolder

Get-ChildItem -Path $diffFolder -Recurse | Remove-Item -Recurse

write-host "unzip Full Publish folder $zip"
Add-Type -assembly "system.io.compression.filesystem"
[io.compression.zipfile]::ExtractToDirectory($zip, $fullPublishFolder)

#Remove-Item $SourceZip 

# Get all files under $source, filter out directories
$firstFolder = Get-ChildItem $fullPublishFolder -Exclude $Excludes | ?{$_.fullname -notmatch '\\API\\web.config'} |  Get-ChildItem -Recurse  | Where-Object { -not $_.PsIsContainer }

$i = 0
$totalCount = $firstFolder.Count
$firstFolder | ForEach-Object {
    $i = $i + 1
    Write-Host -Activity "Searching Files" -status "Searching File  $i of     $totalCount" -percentComplete ($i / $firstFolder.Count * 100)
    # Check if the file, from $source, exists with the same path under $destination
    If ( Test-Path ( $_.FullName.Replace($fullPublishFolder, $currentSite) ) ) {
        # Compare the contents of the two files...
        If ( Compare-Object (Get-FileHash $_.FullName).Hash (Get-FileHash $_.FullName.Replace($fullPublishFolder, $currentSite) ).Hash ) {
            # List the paths of the files containing diffs
             $fileSuffix = $_.FullName.Replace($fullPublishFolder,'')
             $Directory = $diffFolder + $_.DirectoryName.Replace($fullPublishFolder,'')
             if (!(Test-Path $Directory) ) { 
                New-Item -ItemType Directory -Path $Directory 
             }
             Copy-Item $_.FullName -Destination $Directory -Recurse -Container
             
             Write-Host "$fileSuffix is on each server, but does not match Copying updated file to DIff"
        }
    }
    else
    {
        
        $fileSuffix = $_.FullName.Replace($fullPublishFolder,'')
        Write-Host "$fileSuffix is only in source adding new file to Diff"
            if ( $diffFolder -ne $_.DirectoryName)
        {
        $Directory = $diffFolder + $_.DirectoryName.Replace($fullPublishFolder,'')
        }
        else
        {
        $Directory = $diffFolder
        }
             if (!(Test-Path $Directory) ) { 
                $Directory
                New-Item -ItemType Directory -Path $Directory 
             }
             Copy-Item $_.FullName -Destination $Directory -Recurse
               
    }
}

$secondFolder = Get-ChildItem $currentSite -Exclude $Excludes |  Where-Object{$_.fullname -notmatch "\\API\\web.config"} | Get-ChildItem -Recurse | Where-Object { -not $_.PsIsContainer }

$i = 0
$totalCount = $secondFolder.Count
$secondFolder | ForEach-Object {
    $i = $i + 1
    #Write-Progress -Activity "Searching for files only on second folder" -status "Searching File  $i of $totalCount" -percentComplete ($i / $secondFolder.Count * 100)
    # Check if the file, from $folder2, exists with the same path under $folder1
    If (!(Test-Path($_.FullName.Replace($currentSite, $fullPublishFolder))))
    {
        $fileSuffix = $_.FullName.Replace($currentSite,'') | out-file "$deployFolder\deleted.txt" -Append
        
        Write-Host "$fileSuffix is only in Destination"
    }
}

#Generating List of static files for CDN Invalidation

 Get-ChildItem -Path $diffFolder -Recurse -Include @('*.js','*.css','*.jpeg','*.png','*.txt','*.ttf','*.wotf') | % {$_.FullName} | Out-File 'D:\Application\TempPackage\Staticfiles.txt'

 } -ArgumentList $deployFolder, $destinationSite, $SourceZip

 Remove-PSSession $Session