﻿param 
(
    [string]$username = $(throw '- Need path to source package'),
	[string]$password = $(throw '- Need path to destination package'),
    $serverIp = $(throw '- Need path to destination package'),
    [string]$deployFolder = $(throw '- Need path to source package')
	
)



$securePassword = ConvertTo-SecureString $password -AsPlainText -force
$credential = New-Object System.Management.Automation.PsCredential($username,$securePassword)  
$Session = new-PSSession -ComputerName $serverIp -Credential $credential  #this will store session in a variable


Invoke-Command -Session $Session -ScriptBlock { param ($deployFolder)
    write-Host $Session
    $folder = $deployFolder + "\folderList.txt"
    $folderlist = Get-Content $folder
    $diffFolder = $deployFolder + "\Diff\*"
    $FilestoDelete = $deployFolder + "\deleted.txt"
    $backupFolder = 'D:\Application\Backup\'
    
    foreach($folder in $folderlist)
    {
    $backupwebFolder = $folder.Split('\')[-1] + '_'+ $(get-date -format MMddyyyy_hhmmss)
    #New-Item  -Path $backupFolder\$backupwebFolder -ItemType Directory -Force
    Write-Host 'Taking Backup of existing site $folder'
   # Copy-Item -path $folder -Destination $backupFolder\$backupwebFolder -Force -Recurse
    Write-Host "Copying files to $folder from $deployFolder"
    Copy-Item -Recurse -Force -Path $diffFolder -Destination $folder -Verbose
    #Remove-Item $folder\* -Recurse -Include (Get-Content $FilestoDelete) -Verbose 
    }


} -ArgumentList $deployFolder

Remove-PSSession -Session $Session